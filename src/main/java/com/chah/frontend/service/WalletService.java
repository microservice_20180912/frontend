package com.chah.frontend.service;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("wallet")
public interface WalletService {
    @GetMapping("/host/info")
    public Map<String, String> info();
}